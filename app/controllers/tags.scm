;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

;; Controller tags definition of colt
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller tags) ; DO NOT REMOVE THIS LINE!!!

(use-modules (app models posts)
             (colt git)
             (colt config))

(define (gen-content tag)
  (define-syntax-rule (->url url-name)
    (format #f "/archives/~a" (uri-decode url-name)))
  (let ((posts (get-posts-by-tag tag)))
    (tpl->html
     `(div (@ (class "blog-post-content"))
           (h2 "Articles contains tag: "
               (font (@ (class "red-text")) ,tag))
           ,(map (lambda (p)
                   `(p (a (@ (href ,(->url (post-url-name p))))
                          ,(post-title p))))
                 posts)))))

(tags-define
 :tag
 (lambda (rc)
   (let ((content (gen-content (params rc "tag"))))
     (view-render "show" (the-environment)))))

(tags-define
 less/:tag
 (lambda (rc)
   (let ((content (gen-content (params rc "tag"))))
     (view-render "less_show" (the-environment)))))
