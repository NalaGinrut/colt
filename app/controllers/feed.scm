;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2019,2020
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

(define-artanis-controller feed) ; DO NOT REMOVE THIS LINE!!!

(use-modules (colt config)
             (colt utils)
             (colt git)
             (app models posts))

(define (atom-entry post)
  (define-syntax-rule (->url blog-url url-name)
    (format #f "~a/archives/~a/" blog-url (uri-decode url-name)))
  (let* ((meta (post-meta-data post))
         (url-name (post-url-name post))
         (title (meta-data-title meta))
         (updated-time (timestamp->atom-date
                        (string->number
                         (meta-data-timestamp meta))))
         (blog-url (colt-conf-get 'blog-url))
         (author (colt-conf-get 'blog-author))
         (real-url (->url blog-url url-name)))
    `(entry
      (author (name ,author) (uri ,blog-url))
      (title (@ (type "text")) ,(if (string-null? title) "No title" title))
      ;; id must be a full and valid URL
      (id ,real-url)
      (link (@ (rel "alternate") (type "text/html")
               (href ,real-url)))
      (published ,updated-time)
      (updated ,updated-time)
      (content (@ (type "xhtml"))
               (div (@ (xmlns "http://www.w3.org/1999/xhtml"))
                    ,(fix-decode (post-content post)))))))

(define (posts->atom)
  (tpl->html (map atom-entry (get-all-posts #:latest-top? #t))))

(feed-define
 atom
 (options #:cache #t)
 (lambda (rc)
   (let ((blog-name (colt-conf-get 'blog-name))
         (blog-url (colt-conf-get 'blog-url))
         (author (colt-conf-get 'blog-author))
         (update-time (timestamp->atom-date
                       (string->number
                        (get-the-latest-post-update-time))))
         (atom (try-to-get-page-from-cache rc)))
     (response-emit
      (if atom
          atom
          (let ((entries (posts->atom)))
            (cache-this-page rc (view-render "atom" (the-environment)))))
      #:headers '((content-type application/atom+xml))))))
