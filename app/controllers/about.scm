;; Controller about definition of colt
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller about) ; DO NOT REMOVE THIS LINE!!!
(use-modules (app models posts)
             (colt config))

(about-define
 /
 (lambda (rc)
   (let ((about-content (get-intro-content)))
     (view-render "about" (the-environment)))))
