<div class="navbar-fixed">
    <nav class="blue darken-2">
        <div class="nav-wrapper container">
            <a href="https://gitlab.com/NalaGinrut/colt/" class="left colt-icon-button">
                <img src="/img/colt.png" width="40" height="35"
                     title="Powered by Colt Blog Engine" alt="Colt Blog Engine" />
            </a>

            <div class="left blog-name">
                <%= (colt-conf-get 'blog-name) %>
            </div>

            <ul class="right hide-on-med-and-down">
                <li><a href="/index">Home</a></li>
                <li><a href="/about">About</a></li>
                <li><a href="/dashboard">Dashboard</a></li>
                <li>
                    <a href="/feed/atom">Subscribe <img src="/img/feed-icon-14x14.png" alt="[feed]" /></a>
                </li>
            </ul>
        </div>
    </nav>
</div>
