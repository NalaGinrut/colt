
/**
 * Put an example advertisement at the web site.
 *
 * @return {[type]} [description]
 */
(function() {
  var img_url = '/img/lambdachip_adv.gif';
  var ad_tips = 'LambdaChip Ad';


  $(document).ready(function() {

    // detect for home page(list of posts)
    var main = document.querySelector('.main-content');
    if (main) {
      var node = document.createElement('div');
      node.className = 'ad-wrapper ad-wrapper-list';
      node.innerHTML = '<a class="ad" href="https://lambdachip.com/" target="_blank">'
        + '<img src="'+ img_url + '">'
        + '<div class="ad-tips">' + ad_tips + '</div>'
        + '</a>';

      main.insertBefore(node, main.children[1]);
    }

    // detect for post page(detail of article)
    var article = document.querySelector('.article');
    if (article) {
      main = document.querySelector('.blog-post');
      node = document.createElement('div');
      node.className = 'ad-wrapper ad-wrapper-post';
      node.innerHTML = '<a class="ad" href="https://lambdachip.com/" target="_blank">'
        + '<img src="'+ img_url + '">'
        + '<div class="ad-tips">' + ad_tips + '</div>'
        + '</a>';
      main.insertBefore(node, main.querySelector('.blog-post-content'));
    }

  })

})();
