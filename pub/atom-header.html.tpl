<title type="text"><%= blog-name %></title>
<updated><%= update-time %></updated>
<generator uri="https://gitlab.com/NalaGinrut/colt" version="0.0.1">
    Colt Blog Engine
</generator>
<link rel="alternate" type="text/html" href="http://nalaginrut.com/" />
<id><%= blog-url %>/feed/atom</id>
<link rel="self" type="application/atom+xml"
      href='<%= blog-url %>/feed/atom' />
